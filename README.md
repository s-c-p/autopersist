# autoPersist

An attempt to vanish-ify API calls for persistence.

WebApp dev-ing, all is going nice then comes DB storage. Hrmmm...

This intends to be a magical tech that watches your app's mutable(IDK immutable) state variable
And IFF the backend supports OpenAPI (e.g. built with FastAPI), automatically takes care of update to server.

So YOU the webapp dev only plays with what appears to be obj/array containing your app's state. This lib takes care of persistence.

 * js watch a variable (so.com/a/50862441)
 * GraphQL seems interesting
 * JS deep-diff 2 objects, but do remember wild changes are impossible in real webapp the key would remain the same...
 * NO need to store entire db in browser, since view is paginated, keep +1 & -1 @max others get fetched JIT
 * take care of retries (exp/geometrical backoff)






automatic state to api
